# Taiga-front container using nginx-s2i

The builder image should be [registry.gitlab.com/ibotty/s2i-nginx](https://gitlab.com/ibotty/s2i-nginx).

This image overrides the s2i assemble script to download the compiled taiga-front and then uses the regular s2i scripts.

The version of taiga-front to use is configured in `.s2i/environment`.
