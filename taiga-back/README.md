# Taiga-Back container using python s2i

The builder image can be [ubi8/python-38](https://catalog.redhat.com/software/containers/ubi8/python-38/5dde9cacbed8bd164a0af24a), but it should work with any recent-ish s2i python builder.

This image overrides the s2i assemble script to download the taiga-back sources and then uses the regular s2i scripts.

The version of taiga-back to use is configured in `.s2i/environment`.
