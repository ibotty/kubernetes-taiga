#!/bin/bash
set -euo pipefail

shasum_of_url() {
    curl -sSfL "$1" | sha256sum | cut -f1 -d' '
}

TAIGA_BACK_VERSION="$1"
TAIGA_FRONT_DIST_VERSION="$1-stable"

TAIGA_BACK_TARBALL_SHA256SUM="$(shasum_of_url "https://github.com/taigaio/taiga-back/archive/$TAIGA_BACK_VERSION.tar.gz")"
TAIGA_FRONT_DIST_TARBALL_SHA256SUM="$(shasum_of_url "https://github.com/taigaio/taiga-front-dist/archive/$TAIGA_FRONT_DIST_VERSION.tar.gz")"

replace_var() {
    local key value file
    key="$1"
    value="$2"
    for file in */.s2i/environment; do
        sed -i "/$key/s/=.*/=${value}/" "$file"
    done
}

replace_var TAIGA_FRONT_DIST_VERSION "$TAIGA_FRONT_DIST_VERSION"
replace_var TAIGA_FRONT_DIST_TARBALL_SHA256SUM "$TAIGA_FRONT_DIST_TARBALL_SHA256SUM"
replace_var TAIGA_BACK_VERSION "$TAIGA_BACK_VERSION"
replace_var TAIGA_BACK_TARBALL_SHA256SUM "$TAIGA_BACK_TARBALL_SHA256SUM"
